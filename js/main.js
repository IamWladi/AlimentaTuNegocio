$(function (){
  var boton = $('#btn-menu-icon');
  var menuItem = $('menu-item');
  var menu =  $('#btn-menu-content');

  boton.on('click', function(e){
    menuItem.toggleClass('active');
    menu.toggleClass('active');
    e.preventDefault();
  });
    menuItem.on('click', function(e){
    menu.toggleClass('active');
    e.preventDefault();
  });
}())

$(document).ready(function() {
$('.accordion').find('.accordion-toggle').click(function() {
 $(this).next().slideToggle('600');
 $(".accordion-content").not($(this).next()).slideUp('600');
});
$('.accordion-toggle').on('click', function() {
 $(this).toggleClass('active').siblings().removeClass('active');
});
});


$(document).ready(function () {
  var carousel = $("#carousel").waterwheelCarousel({
    flankingItems: 2,
    movingToCenter: function ($item) {
      $('#callback-output').prepend('movingToCenter: ' + $item.attr('id') + '<br/>');
    },
    movedToCenter: function ($item) {
      $('#callback-output').prepend('movedToCenter: ' + $item.attr('id') + '<br/>');
    },
    movingFromCenter: function ($item) {
      $('#callback-output').prepend('movingFromCenter: ' + $item.attr('id') + '<br/>');
    },
    movedFromCenter: function ($item) {
      $('#callback-output').prepend('movedFromCenter: ' + $item.attr('id') + '<br/>');
    },
    clickedCenter: function ($item) {
      $('#callback-output').prepend('clickedCenter: ' + $item.attr('id') + '<br/>');
    }
  });

  $('#prev').bind('click', function () {
    carousel.prev();
    return false
  });

  $('#next').bind('click', function () {
    carousel.next();
    return false;
  });

  $('#reload').bind('click', function () {
    newOptions = eval("(" + $('#newoptions').val() + ")");
    carousel.reload(newOptions);
    return false;
  });

});

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
  anchor.addEventListener('click', function (e) {
    e.preventDefault();

    document.querySelector(this.getAttribute('href')).scrollIntoView({
      behavior: 'smooth'
    });
  });
});
